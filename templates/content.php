<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
  <?php if ( has_post_thumbnail() ) : ?>
        <?php
        $featured_image_id = get_post_thumbnail_id();
        $featured_image    = wp_get_attachment_image_src( $featured_image_id, 'thumbnail' ); ?>

        <img class="alignleft" src="<?= $featured_image[0]; ?>" alt="">
      <?php endif; ?>
    <?php the_excerpt(); ?>
  </div>
</article>
