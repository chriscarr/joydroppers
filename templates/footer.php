<footer class="content-info" role="contentinfo">
  <div class="container footer-subfooter-wrap">
    <div class="col-md-3">
      <?php
        if (has_nav_menu('footer_navigation')) :
          wp_nav_menu(array('theme_location' => 'footer_navigation', 'menu_class' => 'nav nav-footer'));
        endif;
      ?>
    </div>
    <div class="col-md-5">
      <?php

      $args = array(
        'posts_per_page' => 1,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_type' => 'post',
        'post_status' => 'publish' );

        // The Query
        $recent_posts = new WP_Query( $args );

        // The Loop
        while ( $recent_posts->have_posts() ) {
          $recent_posts->the_post();    
          ?>

          <div class="recent-post">
          <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
            the_post_thumbnail('thumbnail');
          } ?>
            <h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More ›</a>
          </div>
        
          <?php
        }
        wp_reset_postdata();
      ?>
    </div>
    <div class="col-md-4">
    <h3><?php the_field('subfoot_form_cta','Options');?><span class="footer-social"><?php
          $socials = array('twitter', 'facebook', 'pinterest', 'instagram');
          foreach ($socials as $social) {
            if (get_field($social, 'Options')) {
              echo '<a href="' . get_field($social, 'Options') . '" target="_blank"><i class="fa fa-' . $social . ($social == 'facebook' ? '-square' : '')  . '"></i></a>';
            }
          }
          ?></span></h3>
      <?php
      if (get_field('subfoot_form_id', 'Options')) {
        if (function_exists('gravity_form')) {
          gravity_form(get_field('subfoot_form_id', 'options'), false, true, false, null, true);
        }
        else if (current_user_can('manage_options')) {
          echo '<div class="alert alert-warning">Please install and activate "Gravity Forms" plugin.</div>';
        }
      }
      ?>
      <p class="form_disclaimer"><?php the_field('subfoot_form_disc','Options'); ?></p>
    </div>
  </div>
  <div class="footer-copyright-wrap">
    <div class="container">
      <div class="footer-disclaimer"><?php the_field('disclaimer', 'options'); ?></div>
      <?php
      $footer_logo = get_field('footer_logo', 'options');
      if ($footer_logo) {
        echo '<img src="' . $footer_logo . '" />';
      }
      ?>
      <p>&copy; <?php echo date('Y') . ' ' . get_field('copyright_line', 'options'); ?></p>
      <?php /*
        $social_network_urls['facebook'] = get_field('facebook', 'option');
        $social_network_urls['twitter'] = get_field('twitter', 'option');
        $social_network_urls['google_plus'] = get_field('google_plus', 'option');
       ?>

      <div class="clearfix">
          <div class="social-actions clearfix">
              <div class="facebook_share social-actions-item">
                  <?php if ( ! empty( $social_network_urls['facebook'] ) ) : ?>
                      <div class="fb-like" data-href="<?= $social_network_urls['facebook']; ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
                  <?php else : ?>
                      <div class="fb-like" data-href="<?php echo esc_url( home_url( '/' ) ); ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
                  <?php endif; ?>
              </div>

              <div class="tweet_share social-actions-item">
                  <?php if ( ! empty( $social_network_urls['twitter'] ) ) : ?>
                      <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?= $social_network_urls['twitter']; ?>" data-count="horizontal" data-via="">Tweet</a>
                  <?php else : ?>
                      <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo esc_url( home_url( '/' ) ); ?>" data-count="horizontal" data-via="">Tweet</a>
                  <?php endif; ?>
              </div>

              <div class="gplus_share social-actions-item">
                  <?php if ( ! empty( $social_network_urls['facebook'] ) ) : ?>
                      <div class="g-plusone" data-size="medium" data-href="<?= $social_network_urls['facebook']; ?>"></div>
                  <?php else : ?>
                      <div class="g-plusone" data-size="medium" data-href="<?php echo esc_url( home_url( '/' ) ); ?>"></div>
                  <?php endif; ?>
              </div>
          </div>
      </div>
      */?>
    </div>
  </div>
</footer>

<?php the_field('conversion_code'); ?>
<?php the_field('tracking_codes', 'options'); ?>

<?php wp_footer(); ?>
<?php if (get_field('subfoot_form_id', 'options') && function_exists('gravity_form_enqueue_scripts')) {
    gravity_form_enqueue_scripts(get_field('subfoot_form_id', 'options'), true);
  } ?>

<?php /*
<script type="text/javascript">
    // Google+
    (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();

    // Twitter
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
</script> */