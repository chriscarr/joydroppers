<header class="banner navbar navbar-default navbar-static-top" role="banner" style="background-image: url(<?php the_field('header_bg', 'Options'); ?>);">
  <div class="header-band">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span><i class="fa fa-bars"></i> <span class="sr-only">Toggle navigation</span> MENU</span>
        </button>
        <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php the_field('logo', 'options'); ?>" alt="<?php bloginfo('name'); ?>" /></a>
      </div>

      <nav class="collapse navbar-collapse" role="navigation">
        <div class="member-header">
          <a href="/members">Member Login ›</a>
        </div>
        <div class="social-header">
          <?php
          $socials = array('twitter', 'facebook', 'pinterest', 'instagram');
          foreach ($socials as $social) {
            if (get_field($social, 'Options')) {
              echo '<a href="' . get_field($social, 'Options') . '" target="_blank"><i class="fa fa-' . $social . ($social == 'facebook' ? '-square' : '')  . '"></i></a>';
            }
          }
          ?>
          </div>
          <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav navbar-right'));
          endif;
        ?>
      </nav>
    </div>
  </div>
</header>
