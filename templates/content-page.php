<?php

if (have_rows('page_layout')) {
  $layout_count = array();
  while ( have_rows('page_layout') ) : the_row();
    $row_layout = get_row_layout();
    $layout_count[$row_layout] = array_key_exists($row_layout, $layout_count) ? $layout_count[$row_layout] + 1 : 1;
    if (file_exists(TEMPLATEPATH . '/layouts/' . $row_layout . '.php')) {
      $page_layout_class = 'page-layout page-layout-' . $row_layout . '-' . $layout_count[$row_layout] . ' page-layout-' . $row_layout;
      $page_layout_variation = get_sub_field('variation');
      if ($page_layout_variation) {
        $page_layout_class .= ' page-layout-variation-' . $page_layout_variation;
      }
      echo '<div class="' . $page_layout_class . '" id ="page-layout-' . $row_layout . '-' . $layout_count[$row_layout] . '">';
      include TEMPLATEPATH . '/layouts/' . $row_layout . '.php';
      echo '</div>';
    }
    elseif (current_user_can('edit_pages')) {
      echo '<div class="container"><div class="alert alert-warning">Can\'t find layout for ' . $row_layout . ' (e.g. ' . TEMPLATEPATH . '/layouts/' . $row_layout . '.php)</div></div>';
    }
  endwhile;
}

?>
<?php // the_content(); ?>
<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>