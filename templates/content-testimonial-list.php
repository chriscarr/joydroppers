<?php

$types = get_terms( 'testimonial-type', array(
  'orderby'    => 'date',
  'hide_empty' => 0
 ) );

if ($types && !empty($types)) {
  echo '<div class="row testimonial-types-listing">';
  foreach ($types as $type) {
    $term_link = get_term_link( $type );
    if (!is_wp_error($term_link)) {
      echo '<div class="col-sm-3">';
      echo '<div class="testimonial-term-list-item">';
      echo '<a href="' . esc_url( $term_link ) . '">';
      $term_img = get_field('image', 'testimonial-type_' . $type->term_id);
      if ($term_img) {
        echo '<img src="' . $term_img['sizes']['medium'] . '" />';
      }
      echo '<span class="term-title">' . $type->name . '</span></a>';
      echo '</div>';
      echo '</div>';
    }
  }
  echo '</div>';
}  