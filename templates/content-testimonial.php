<?php 

global $post;

$testimonial_photo = get_field('photo');
$testimonial_location = get_field('location');
$testimonial_description = get_field('description');

?>
<div class="testimonial clearfix">
  <img src="<?php echo $testimonial_photo['url']; ?>" alt="<?php the_title(); ?>" />
  <div class="testimonial-stats">
    <span class="testimonial-title"><?php the_title(); ?></span>
    <span class="testimonial-location"><?php echo $testimonial_location; ?></span>
    <span class="testimonial-description"><?php echo $testimonial_description; ?></span>
    <span class="testimonial-type"><strong>Testimonial Type: </strong><?php
      $types = get_the_terms( $post->id, 'testimonial-type' );
      $first = true;
      if ($types && !empty($types)) {
        foreach ($types as $type) {
          $term_link = get_term_link( $type );
          if (!is_wp_error($term_link)) {
            if (!$first) {
              echo ', ';
            }
            else {
              $first = false;
            }
            echo '<a href="' . esc_url( $term_link ) . '">' . $type->name . '</a>';
          }
        }
      }
      ?></span>
  </div>
  <div class="testimonial-content"><?php the_content(); ?></div>
</div>