<div class="wrap" role="document">
  <div class="content">
    <main class="main" role="main">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <div class="well">
              <h3 style="margin-top: 0;">Member Sign In</h3>
              <p><small><em>Already have an account? Sign in!</em></small></p>
              <?php
                $args = array(
                    'echo'           => true,
                    'redirect'       => get_bloginfo('wpurl') . '/members/?signin=1', 
                    'form_id'        => 'loginform',
                    'label_username' => __( 'Username' ),
                    'label_password' => __( 'Password' ),
                    'label_remember' => __( 'Remember Me' ),
                    'label_log_in'   => __( 'Log In ›' ),
                    'id_username'    => 'user_login',
                    'id_password'    => 'user_pass',
                    'id_remember'    => 'rememberme',
                    'id_submit'      => 'wp-submit',
                    'remember'       => true,
                    'value_username' => NULL,
                    'value_remember' => true
                );
                wp_login_form($args);
              ?>
            </div>
          </div>
          <div class="col-sm-4">
            <h3><strong>Are you a new team member, and need a user account?</strong></h3>
            <p>Contact us and we will set one up for you!</p>
            <p><a href="/contact" class="btn btn-warning">Request Member Account ›</a></p>
          </div>
        </div>
      </div>
    </main><!-- /.main -->
  </div><!-- /.content -->
</div><!-- /.wrap -->