<?php

global $post;
$author = get_userdata($post->post_author);
$author = $author->display_name;

?>
<p class="entry-meta"><time class="published" datetime="<?php echo get_the_time('c'); ?>"><?php echo get_the_date(); ?></time> <span class="byline author vcard"><?php echo __('by', 'roots'); ?> <a href="<?php echo get_author_posts_url($post->post_author); ?>" rel="author" class="fn"><?php echo $author; ?></a></span></p>
