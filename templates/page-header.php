<?php if (!is_front_page() && !get_field('hide_page_header')) {
  if (has_post_thumbnail()) {
    $thumb_id = get_post_thumbnail_id();
    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
    $hero_background = $thumb_url_array[0];
  }
  else {
    $hero_background = get_field('default_page_header', 'options');
    if (!$hero_background && current_user_can('manage_options')) {
      echo '<div class="alert alert-warning" style="margin-top: 30px;">Please set a default page header image in the site settings.</div>';
    }
  }
  ?>
<div class="page-header">
  <div class="container">
    <h1>
      <?php echo roots_title(); ?>
    </h1>
    <?php if ( function_exists('yoast_breadcrumb') ) {
      yoast_breadcrumb('<p id="breadcrumbs">','</p>');
    } ?>
  </div>
</div>
<?php } ?>
