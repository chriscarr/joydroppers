<?php
/*
Template Name: Protected Members Content
*/
?>
<?php
  if (is_user_logged_in()) {
    if (current_user_can('partner') || current_user_can('manage_options')) {
      while (have_posts()) : the_post();
        get_template_part('templates/content', 'page');
      endwhile;
    }
    else {
      ?>
      
      <div class="alert alert-warning">Sorry, you don't have permission to access this content.</div>

      <?php
    }
  }
  else {
    get_template_part('templates/content', 'signuppage');
  }
?>
