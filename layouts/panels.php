<?php if (have_rows('panels')): ?>

  <div class="container">
    <div class="row">

    <?php while( have_rows('panels') ): the_row(); 

      $image    = get_sub_field('image');
      $subtitle = get_sub_field('subtitle');
      $button   = get_sub_field('button_text');
      $link     = get_sub_field('link');

      ?>

      <div class="col-sm-4">
        <?php
        if ($link) {
          echo '<a href="' . $link . '">';
        }
        if (!empty($image)) {
          echo '<img src="' . $image['url'] . '" alt="' . $image['alt'] . '" />';
        }
        if ($subtitle) {
          echo '<p>' . $subtitle . ' <i class="fa fa-long-arrow-right"></i></p>';
        }
        if ($link) {
          echo '</a>';
        }

      ?>
      </div>

    <?php endwhile; ?>

    </div>
  </div>

<?php endif; ?>