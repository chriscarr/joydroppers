<div class="container">
  <div class="row-fluid"><?php
  $columns = get_sub_field('number_of_columns');
  if ($columns == '2') {
    $colclass = "col-sm-6";
  }
  else if ($columns == '3') {
    $colclass = "col-sm-4";
  }
  else if ($columns == '4') {
    $colclass = "col-sm-3";
  }
  for ($i = 1; $i <= $columns; $i++) { 
    echo '<div class="' . $colclass . '">'  . get_sub_field('column_' . $i) . '</div>';  
  }
  echo get_sub_field('content');

  ?></div>
</div>