<?php

    $timeline_heading = get_sub_field('timeline_heading');
    if ($timeline_heading) {
      echo '<div class="timeline-heading">' . $timeline_heading . '</div>';
    }
    

 if (have_rows('entry')): ?>

  <div class="flexslider flexslider-timeline">
    <ul class="slides">

    <?php while (have_rows('entry')): the_row(); 

      $entry_date    = get_sub_field('date');
      $entry_content = get_sub_field('content');
      
      ?>

      <li data-date="<?php echo $entry_date; ?>">
        <?php
        if ($entry_date) {
          echo '<h4>' . $entry_date . '</h4>';
        }
        if ($entry_content) {
          echo '<div class="timeline_entry_content">' . $entry_content . '</div>';
        }

      ?>
      </li>

    <?php endwhile; ?>

    </ul>
  </div>
  <?php if (!isset($timelinejs)) {
    $timelinejs = true;
    ?>
    <script>
      jQuery(window).load(function() {
        jQuery('.flexslider-timeline').flexslider({
          animation: "slide",
          prevText: '<i class="fa fa-long-arrow-left"></i> Prev',
          nextText: 'Next <i class="fa fa-long-arrow-right"></i>',
          animationLoop: false,
          smoothHeight: true,
          slideshow: false
        });
        var index;
        jQuery('.flexslider-timeline .slides li').each(function( index ) {
          var numindex;
          numindex = index + 1;
          jQuery('.flex-control-paging li:nth-child(' + numindex + ') a').append('<span class="timeline-date">' + jQuery(this).data('date') + '</span>');

        });
      });
      </script>
  <?php
  }
endif; ?>