<div class="vidwrap" style="background-image: url(<?php echo get_sub_field('image'); ?>)">
  <video loop="loop" autoplay="autoplay" class="hidden-sm hidden-xs">
    <source type="video/webm" src="<?php echo get_sub_field('webm'); ?>" />
    <source type="video/mp4" src="<?php echo get_sub_field('h264'); ?>" />
  </video>
</div>
<?php 

$video_overlay = get_sub_field('content');
if ($video_overlay) {
  echo '<div class="container"><div class="video_overlay">' . $video_overlay . '</div></div>';
}