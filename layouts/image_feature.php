<?php
$width        = get_sub_field('width');
$type         = get_sub_field('type');
$features[1]  = get_sub_field('feature_1');
$features[1]  = $features[1][0];
if ($type) {
  $features[2] = get_sub_field('feature_2');
  $features[2] = $features[2][0];
}
if (!$width) {
  echo '<div class="container">';
}
  echo '<div class="image-features ' . ($type ? 'image-features-dual' : 'image-features-single') . '">';
    foreach ($features as $featurenum => $feature) {
      echo '<div class="image-feature image-feature-' . $featurenum . '">';
        echo '<img src="' . $feature['background_image']['url'] . '" alt="' . $feature['background_image']['alt'] . '" />';
        if ($feature['content']) {
          echo '<div class="feature-content">';
            echo $feature['content'];
            if ($feature['link'] && $feature['button']) {
              echo '<p><a href="' . $feature['link'] . '" class="btn btn-primary">' . $feature['button'] . '</a></p>';
            }
          echo '</div>';
        }
      echo '</div>';
    }
  echo '</div>';
if (!$width) {
  echo '</div>';
}