<div class="container">
  <?php

  if (get_sub_field('type')) {
    $testimonial_id = get_sub_field('testimonial_post');
    $testimonial = get_post($testimonial_id);
    //echo '<pre>'; print_r($testimonial); echo '</pre>';

    $testimonial_photo        = get_field('photo', $testimonial_id);
    $testimonial_location     = get_field('location', $testimonial_id);
    $testimonial_description  = get_field('description', $testimonial_id);
    ?>
    
    <div class="testimonial">
      <?php if ($testimonial_photo) { ?>
      <img src="<?php echo $testimonial_photo['url']; ?>" alt="<?php the_title(); ?>" />
      <?php } ?>
      <div class="testimonial-stats">
        <span class="testimonial-title"><?php echo $testimonial_id->post_title; ?></span>
        <span class="testimonial-location"><?php echo $testimonial_location; ?></span>
        <span class="testimonial-description"><?php echo $testimonial_description; ?></span>
      </div>
      <div class="testimonial-content"><?php echo $testimonial->post_content ?></div>
    </div>

  <?php
  }
  else {
    $args = array(
      'post_type' => 'testimonials',
      'posts_per_page' => '4',
    );
    $the_query = new WP_Query( $args );

    if ($the_query->have_posts()) { 
      ?>

      <div class="flexslider testimonials-slideshow">
        <ul class="slides">
          <?php
            while ($the_query->have_posts()) {
              $the_query->the_post();
              $testimonial_photo = get_field('photo');
              $testimonial_location = get_field('location');
              $testimonial_description = get_field('description');
              ?>
              <li>
                <div class="testimonial clearfix">
                  <?php if ($testimonial_photo) { ?>
                  <img src="<?php echo $testimonial_photo['url']; ?>" alt="<?php the_title(); ?>" />
                  <?php } ?>
                  <div class="testimonial-stats">
                    <span class="testimonial-title"><?php the_title(); ?></span>
                    <span class="testimonial-location"><?php echo $testimonial_location; ?></span>
                    <span class="testimonial-description"><?php echo $testimonial_description; ?></span>
                  </div>
                  <div class="testimonial-content"><?php the_excerpt(); ?></div>
                </div>
              </li>
              <?php
            } // end while
            ?>
        </ul>
      </div>
      <?php 
      if ($the_query->post_count > 1) {
      ?>
      <script>
        jQuery(window).load(function() {
          jQuery('.flexslider').flexslider({
            controlNav: false,
            directionNav: false,
            pauseOnHover: true,  
            animation: "fade"
          });
        });
      </script>

      <?php
      }
    }
    else if (current_user_can('manage_options')) {
      echo '<div class="alert alert-warning"><strong>Admin note:</strong> No testimonials found, add some in the site admin.</div>';
    }
    wp_reset_postdata();
  }

  ?>

</div>