<?php

$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));

?>
<div class="container">
  <div class="row">
    <div class="col-sm-9 partner-main">
      <p id="breadcrumbs">
        <span prefix="v: http://rdf.data-vocabulary.org/#">
          <span typeof="v:Breadcrumb"><a href="http://jd.c2itllc.com" rel="v:url" property="v:title"><i class="fa fa-home"></i></a></span> / <span typeof="v:Breadcrumb"><a href="/meet-the-joydroppers-team/" rel="v:url" property="v:title">Team Members</a></span> / <span typeof="v:Breadcrumb"><span class="breadcrumb_last" property="v:title"><?php echo $curauth->display_name; ?></span></span>
        </span>
      </p>
      <h1 class="partner-title"><?php echo $curauth->display_name; ?></h1>
      <h4><?php echo get_field('partner_city', 'user_' . $curauth->ID) ? get_field('partner_city', 'user_' . $curauth->ID) . ', ' : '';
        echo get_field('partner_state', 'user_' . $curauth->ID); ?> | Member Level: <?php the_field('partner_level', 'user_' . $curauth->ID); ?></h4>
      <?php 
      $partner_photo = get_field('partner_photo', 'user_' . $curauth->ID);
      echo '<img src="' . $partner_photo['sizes']['thumbnail'] . '" class="partner-photo alignleft" />';
      the_field('partner_bio', 'user_' . $curauth->ID); ?>
      <?php
       while (have_posts()) : the_post(); ?>
          <?php get_template_part('templates/content', get_post_format()); ?>
        <?php endwhile; ?>

        <?php if ($wp_query->max_num_pages > 1) : ?>
          <nav class="post-nav">
            <ul class="pager">
              <li class="previous"><?php next_posts_link(__('&larr; Older posts', 'roots')); ?></li>
              <li class="next"><?php previous_posts_link(__('Newer posts &rarr;', 'roots')); ?></li>
            </ul>
          </nav>
        <?php endif; ?>

      <a href="mailto:<?php echo $curauth->user_email; ?>" class="btn btn-primary btn-sm"><i class="fa fa-envelope" style="margin-right: 5px;"></i> Email Me ›</a> 

    </div>
    <div class="col-sm-3 partner-list partner-list-sidebar">
      <h3>All Members</h3>
      <?php
      
      $blogusers = get_users(
        array(
          'role'=> 'partner',
         )
      );

      // Array of stdClass objects.
      foreach ( $blogusers as $user ) {
        $partner_photo = get_field('partner_photo', 'user_' . $user->ID);
        echo '<a href="' . get_author_posts_url( $user->ID ) . '"><img src="' . $partner_photo['sizes']['thumbnail'] . '" />' . esc_html( $user->display_name ) . '<span>' . (get_field('partner_city', 'user_' . $user->ID) ? get_field('partner_city', 'user_' . $user->ID) . ', ' : '') . get_field('partner_state', 'user_' . $user->ID) . ' | Level: ' . get_field('partner_level', 'user_' . $user->ID) . '</span></a>';
      } 

      ?>
    </div>
  </div>
</div>