<?php
/**
 * Clean up the_excerpt()
 */
function roots_excerpt_more($more) {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'roots') . '</a>';
}
add_filter('excerpt_more', 'roots_excerpt_more');

/**
 * Manage output of wp_title()
 */
function roots_wp_title($title) {
  if (is_feed()) {
    return $title;
  }

  $title .= get_bloginfo('name');

  return $title;
}
add_filter('wp_title', 'roots_wp_title', 10);

if (function_exists('acf_add_options_page')) {
  acf_add_options_page('Site Options');
}

function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

/**
 * Fix nav menu active classes for custom post types
 */
function roots_cpt_active_menu($menu) {
  if ('tribe_events' === get_post_type()) {
    $menu = str_replace('active', '', $menu);
    $menu = str_replace('menu-events', 'menu-events active', $menu);
  }
  if ('testimonial' === get_post_type() || is_author()) {
    $menu = str_replace('active', '', $menu);
  }
  return $menu;
}
add_filter('nav_menu_css_class', 'roots_cpt_active_menu', 400);

// Customize user editor

add_action( 'personal_options', array ( 'jd_Hide_Profile_Bio_Box', 'start' ) );

class jd_Hide_Profile_Bio_Box {
  public static function start() {
    $action = ( IS_PROFILE_PAGE ? 'show' : 'edit' ) . '_user_profile';
    add_action( $action, array ( __CLASS__, 'stop' ) );
      ob_start();
  }

  public static function stop() {
    $html = ob_get_contents();
    ob_end_clean();

    // remove the headline
    $headline = __( IS_PROFILE_PAGE ? 'About Yourself' : 'About the user' );
    $html = str_replace( '<h3>' . $headline . '</h3>', '', $html );

    // remove the table row
    $html = preg_replace( '~<tr>\s*<th><label for="description".*</tr>~imsUu', '', $html );
    print $html;
  }
}

// Remove social links from user editor

function modify_contact_methods($profile_fields) {
  unset($profile_fields['twitter']);
  unset($profile_fields['facebook']);
  unset($profile_fields['googleplus']);
  return $profile_fields;
}
add_filter('user_contactmethods', 'modify_contact_methods');

// Change Author url base to "partner"

add_action('init', 'cng_author_base');
function cng_author_base() {
    global $wp_rewrite;
    $author_slug = 'partner'; // change slug name
    $wp_rewrite->author_base = $author_slug;
}

function jd_is_posttype($posttypes) {
  foreach ($posttypes as $posttype) {
    if (get_post_type() == $posttype) {
      return true;
    }
  }
  return false;
}

// Dequeue "search everything" plugin stylesheet
function jd_dequeue_styles() {
  wp_dequeue_style('se-link-styles');
}
add_action('wp_enqueue_scripts', 'jd_dequeue_styles', 100);

// Hide ACF admin
function c2_isdeveloper() {  
  $current_user = wp_get_current_user();
  if ($current_user->user_login == 'chris') {
    return true;
  }
  return false;
}
add_filter('acf/settings/show_admin', 'c2_isdeveloper');