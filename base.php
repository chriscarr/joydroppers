<?php
  if (!function_exists('the_field')) {
    echo 'This site requires <a href="http://www.advancedcustomfields.com/pro/">Advanced Custom Fields Pro</a> (v.5 or greater). Please activate that plugin and try again.';
    die;
  }
?>

<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

  <!--[if lt IE 9]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
  <![endif]-->
<?php /*
  <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=640899279268843";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
*/ ?>
  <?php
    do_action('get_header');
    get_template_part('templates/header');
  ?>

  <div class="wrap" role="document">
    <?php if (get_post_type() != 'post' && get_post_type() != 'tribe_events' && get_post_type() != 'tribe_venue' && !is_author() ) {
      get_template_part('templates/page', 'header');
    } ?>
    <div class="container<?php echo roots_display_sidebar() ? '' : '-fluid'; ?>">
      <div class="content row">
        <main class="main" role="main">
          <?php include roots_template_path(); ?>
        </main><!-- /.main -->
        <?php if (roots_display_sidebar()) : ?>
          <aside class="sidebar" role="complementary">
            <?php include roots_sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.container -->
  </div><!-- /.wrap -->

  <?php get_template_part('templates/footer'); ?>

</body>
</html>
